
package primer.proyecto;


public class Persona {
     private String nombre;
    private String apellido;
    private String ciudad ;
    private String universidad;
    private int añona;
    private int mat;
    
    public Persona(String n, String a, String c, String u , int an, int ma){
        nombre=n;
        apellido=a;
        ciudad=c;
        universidad=u;
        añona = an;
        mat=ma;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public int getAñona() {
        return añona;
    }

    public void setAñona(int añona) {
        this.añona = añona;
    }

    public int getMat() {
        return mat;
    }

    public void setMat(int mat) {
        this.mat = mat;
    }
    
    
}


