package primer.proyecto;

public class Alumno extends Persona {

    private int edificio;
    private int edu;

    public Alumno(String n, String a, String c, String u, int edificio, int añona, int mat) {
        super(n, a, c, u, añona,mat);
        this.edificio  = edificio;
    }       

    public int getEdificio() {
        return edificio;
    }

    public void setEdificio(int edificio) {
        this.edificio = edificio;
    }

    public String toString() {
        String a = " \n Mi nombre es : " + getNombre() + "\n Mi apellido es :" + getApellido() + "\n Mi ciudad es: " + getCiudad() + "\n Mi Universidad es :" + getUniversidad()+"\n Naci en el año "+getAñona()+"\n Me encuentro en el edificio: "+getEdificio()+"\n Tengo "+getMat()+" Materias";
        return a;
    }
}
