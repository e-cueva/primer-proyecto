package primer.proyecto;

public class Licenciado extends Persona {

    private String carrera;

    public Licenciado(String n, String a, String c, String u, String tit, int añona, int ma) {
        super(n, a, c, u, añona, ma);
        this.carrera = tit;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String toString() {
        String p = " DATOS PROFESOR." + "\n" + " \n EL nombre es : " + getNombre() + "\n Mi apellido es :" + getApellido() + "\n Mi ciudad es: " + getCiudad() + "\n Mi Carrera es :" + getCarrera()+"\n Enseño "+getMat()+" materias";
        return p;
    }
}
